# zadanie_3_gr_1.2_alektom025

%reads data files
f1 = 'C:\Users\student\Desktop\data\dane-pomiarowe_2019-10-08.csv'
f2 = 'C:\Users\student\Desktop\data\dane-pomiarowe_2019-10-09.csv'
f3 = 'C:\Users\student\Desktop\data\dane-pomiarowe_2019-10-10.csv'
f4 = 'C:\Users\student\Desktop\data\dane-pomiarowe_2019-10-11.csv'
f5 = 'C:\Users\student\Desktop\data\dane-pomiarowe_2019-10-12.csv'
f6 = 'C:\Users\student\Desktop\data\dane-pomiarowe_2019-10-13.csv'
f7 = 'C:\Users\student\Desktop\data\dane-pomiarowe_2019-10-14.csv'
file1 = fopen(f1)
a = textscan(file1, '%q %q %q %q %q %q', 'Delimiter', ';')
file2 = fopen(f2)
b = textscan(file2, '%q %q %q %q %q %q', 'Delimiter', ';')
file3 = fopen(f3)
c = textscan(file3, '%q %q %q %q %q %q', 'Delimiter', ';')
file4 = fopen(f4)
d = textscan(file4, '%q %q %q %q %q %q', 'Delimiter', ';')
file5 = fopen(f5)
e = textscan(file5, '%q %q %q %q %q %q', 'Delimiter', ';')
file6 = fopen(f6)
f = textscan(file6, '%q %q %q %q %q %q', 'Delimiter', ';')
file7 = fopen(f7)
g = textscan(file7, '%q %q %q %q %q %q', 'Delimiter', ';')
%creates a matrix Day with date in scientific format and concentration of PM10
for i=2:25
Day(i-1,1)=datenum(strcat('2019-10-08',{' '},cell2mat(a{1}(i))));
Day(i-1,2)=str2num(cell2mat(a{6}(i)));
Day(i-1+24,1)=datenum(strcat('2019-10-09',{' '},cell2mat(b{1}(i))));
Day(i-1+24,2)=str2num(cell2mat(b{6}(i)));
Day(i-1+48,1)=datenum(strcat('2019-10-10',{' '},cell2mat(c{1}(i))));
Day(i-1+48,2)=str2num(cell2mat(c{6}(i)));
Day(i-1+72,1)=datenum(strcat('2019-10-11',{' '},cell2mat(d{1}(i))));
Day(i-1+72,2)=str2num(cell2mat(d{6}(i)));
Day(i-1+96,1)=datenum(strcat('2019-10-12',{' '},cell2mat(e{1}(i))));
Day(i-1+96,2)=str2num(cell2mat(e{6}(i)));
Day(i-1+120,1)=datenum(strcat('2019-10-13',{' '},cell2mat(f{1}(i))));
Day(i-1+120,2)=str2num(cell2mat(f{6}(i)));
Day(i-1+144,1)=datenum(strcat('2019-10-09',{' '},cell2mat(g{1}(i))));
Day(i-1+144,2)=str2num(cell2mat(g{6}(i)));
end;
%creates a folder and prints the maximum concentration together with the date in it
fid= fopen('PMresults.dat', 'wt');
fprintf(fid, 'Analysed time: \n from ');
fprintf(fid, datestr(Day(1,1)));
fprintf(fid, ' to ');
fprintf(fid, datestr(Day(168,1)));
fprintf(fid, ' 24:00:00 ');
M = max(Day(:,2));
[x,y] = find(Day == M);
fprintf(fid, ' \n on ');
fprintf(fid, datestr(Day(x)));
fprintf(fid, ' the maximum concentration was: %.2f\n', M);
fclose(fid);
%prints the maximum concentration with the date
fprintf('Analysed time: \n from ');
fprintf(datestr(Day(1,1)));
fprintf(' to ');
fprintf(datestr(Day(168,1)));
fprintf(' 24:00:00 ');
fprintf(' \n on ');
fprintf(datestr(Day(x)));
fprintf(' the maximum concentration was: %.2f\n', M);

%sum of concentrations during the days and at nights

sigma8n = 0;

sigma7d = 0;
sigma7n = 0;

sigma6d = 0;
sigma6n = 0;

sigma5d = 0;
sigma5n = 0;

sigma4d = 0;
sigma4n = 0;

sigma3d = 0;
sigma3n = 0;

sigma2d = 0;
sigma2n = 0;

sigma1d = 0;
sigma1n = 0;
%sums of values
for n8=162:168;
sigma8n = sigma8n + Day(n8,2);
end;

for d7=151:161;
sigma7d = sigma7d + Day(d7,2);
end;
for n7=138:150;
sigma7n = sigma7n + Day(n7,2);
end;

for d6=127:137;
sigma6d = sigma6d + Day(d6,2);
end;
for n6=114:126;
sigma6n = sigma6n + Day(n6,2);
end;

for d5=103:113;
sigma5d = sigma5d + Day(d5,2);
end;
for n5=90:102;
sigma5n = sigma5n + Day(n5,2);
end;

for d4=79:89;
sigma4d = sigma4d + Day(d4,2);
end;
for n4=66:78;
sigma4n = sigma4n + Day(n4,2);
end;

for d3=55:65;
sigma3d = sigma3d + Day(d3,2);
end;
for n3=42:54;
sigma3n = sigma3n + Day(n3,2);
end;

for d2=31:41;
sigma2d = sigma2d + Day(d2,2);
end;
for n2=18:30;
sigma2n = sigma2n + Day(n2,2);
end;

for d1=7:17;
sigma1d = sigma1d + Day(d1,2);
end;
for n1=1:6;
sigma1n = sigma1n + Day(n1,2);
end;
%averages for the night
sr1n = sigma1n/6;
sr2n = sigma2n/13;
sr3n = sigma3n/13;
sr4n = sigma4n/13;
sr5n = sigma5n/13;
sr6n = sigma6n/13;
sr7n = sigma7n/13;
sr8n = sigma8n/7;
%average for the night
sr1d = sigma1d/11;
sr2d = sigma2d/11;
sr3d = sigma3d/11;
sr4d = sigma4d/11;
sr5d = sigma5d/11;
sr6d = sigma6d/11;
sr7d = sigma7d/11;
%matrix from average days
D(1,1) = sr1d;
D(2,1) = sr2d;
D(3,1) = sr3d;
D(4,1) = sr4d;
D(5,1) = sr5d;
D(6,1) = sr6d;
D(7,1) = sr7d;	

sr1_7d = sum(D)/7;
%matrix from average nights
N(1,1) = sr1n;
N(2,1) = sr2n;
N(3,1) = sr3n;
N(4,1) = sr4n;
N(5,1) = sr5n;
N(6,1) = sr6n;
N(7,1) = sr7n;
%average of all nights
sr1_7n = sum(N)/7;

pop1 = 7; %population 1
pop2 = 7; %population 2	
%variation for days
VAR1 =((D(1,1)-sr1_7d)^2+(D(2,1)-sr1_7d)^2+(D(3,1)-sr1_7d)^2+(D(4,1)-sr1_7d)^2+(D(5,1)-sr1_7d)^2+(D(6,1)-sr1_7d)^2+(D(7,1)-sr1_7d)^2)/pop1;
%variation for nights
VAR2 =((N(1,1)-sr1_7n)^2+(N(2,1)-sr1_7n)^2+(N(3,1)-sr1_7n)^2+(N(4,1)-sr1_7n)^2+(N(5,1)-sr1_7n)^2+(N(6,1)-sr1_7n)^2+(N(7,1)-sr1_7n)^2)/pop2;
%standard deviation
s = sqrt((((pop1-1)*VAR1+(pop2-1)*VAR2)/pop1+pop2-2)*((1/pop1)+(1/pop2)));
T = abs(sr1_7d-sr1_7n)/s;
fprintf(fid, 'With an assumed degree of freedom equal to 7, \n');
fprintf(fid, ' the value of the student's t distribution is ');
fprintf(fid, T);
fprintf(fid, '\n i.e. the result is not statistically significant, \n');

fprintf('With an assumed degree of freedom equal to 7, \n');
fprintf(' the value of the student's t distribution is ');
fprintf(T);
fprintf('\n i.e. the result is not statistically significant, \n');
